<h1>Edit Comment</h1>
<?php
echo $this->Form->create('comment', array('enctype'=>'multipart/form-data'));
echo $this->Form->input('comment', array('label'=>false, 'rows' => '2', 'placeholder' => 'Edit your a comment'));
echo $this->Form->input('upload', array('type'=>'file', 'label' => 'photo comment', )); 
echo $this->Form->input('post_id', array('type' => 'hidden'));
echo $this->Form->end('Save Post');
?>