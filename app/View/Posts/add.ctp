<!-- File: /app/View/Posts/add.ctp -->

<h1>Add Post</h1>
<?php
echo $this->Form->create('Post',array('enctype'=>'multipart/form-data'));
echo $this->Form->input('title');
echo $this->Form->input('post', array('rows' => '3', 'label' => 'Pls write your post'));
echo $this->Form->input('upload', array('type'=>'file', 'label' => 'Upload your photo', ));
echo $this->Form->end('Save Post');
?>
<?php
echo $this->Html->link('Back',array('controller' => 'Posts', 'action' => 'index'));
?>
 