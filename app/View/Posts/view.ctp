<div id ="content">
<h1><font size='5'><?php echo $posts['Post']['title'] ; ?></font></h1>
<hr>
<center><?php echo $this->html->image($posts['Post']['photo'], array('width' => '800', 'height' => '500')); ?></center>
<br>
<p><?php echo $posts['Post']['post'] ; ?></p>
<p><small>Created: <?php echo $posts['Post']['created']; ?></small></p>
<table>
<?php foreach ($com as $comment_arr): ?>
	<tr>
        <td><?php 
        		echo $comment_arr['comment']['name'].":".$comment_arr['comment']['comment']; 
        	?>
            <center><?php
                echo $this->html->image($comment_arr['comment']['image'], array('width' =>'250', 'height' => '250'));
                ?>
            </center>
        </td>
        <td>
            <?php
            	$name = $this->Session->read('Auth.User.username');
                //$name = $this->Auth->user('usename');
            	if ($comment_arr['comment']['name'] === $name){
            		echo $this->Form->postLink(
                    	'Delete',
                    	array('controller' => 'Comments','action' => 'commentdelete',$comment_arr['comment']['id'],
                            $comment_arr['comment']['posts_id']),
                    	array('confirm' => 'Are you sure?')
                	);
                	echo " | ";
                	echo $this->Html->link(
                    	'Edit', array('controller' => 'Comments','action' => 'editcomment', $comment_arr['comment']['id']));
            	}
            ?>   
        </td>
    </tr> 
<?php endforeach; ?>
</table>
  
<?php echo $this->Form->create('comment',array('enctype'=>'multipart/form-data','url' => array ('controller' => 'Comments', 'action' => 'addcomment'))); 
echo $this->Form->input('comment', array('label'=>false, 'rows' => '2', 'placeholder' => 'Write a comment')); 
echo $this->Form->input('posts_id', array('type' => 'hidden', 'value' => $posts['Post']['id']));
echo $this->Form->input('upload', array('type'=>'file', 'label' => 'photo comment', )); 
echo $this->Form->end('Add comment'); ?>

<?php echo $this->Html->link("Back",array("controller" => "Posts", "action" => "index")); ?>