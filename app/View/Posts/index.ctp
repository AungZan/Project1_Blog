<!-- File: /app/View/Posts/index.ctp -->
<div id ="content">
<?php echo $this->Session->flash(); ?>
<h1>Blog posts</h1>
<?php echo $this->Html->link(
    'Add Post',
    array('controller' => 'posts', 'action' => 'add')
); ?>
<table>
    <tr>
        <th>Title of the post</th>
        <th>Action</th>
        <th>Created</th>
    </tr>
<!-- Here's where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
    <tr>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                $au_user_id = $this->Session->read("Auth.User.id");
                //$au_user_id = $this->Auth->user('id');
                if ($post['Post']['user_id']===$au_user_id){
                    echo $this->Form->postLink(
                        'Delete',
                        array('action' => 'delete', $post['Post']['id']),
                        array('confirm' => 'Are you sure?')
                    );
                    echo " | ";
                    echo $this->Html->link(
                        'Edit', array('action' => 'edit', $post['Post']['id'])
                    );
                }

            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>
<?php echo $this->Html->link(
    'Logout',
    array('controller' => 'Users', 'action' => 'login')
); ?>
    