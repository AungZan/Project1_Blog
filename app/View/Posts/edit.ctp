<!-- File: /app/View/Posts/edit.ctp -->

<h1>Edit Post</h1>
<?php
echo $this->Form->create('Post', array('enctype'=>'multipart/form-data'));
echo $this->Form->input('title');
echo $this->Form->input('post', array('rows' => '3'));
echo $this->Form->input('upload', array('type'=>'file', 'label' => 'Upload your photo'));
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->end('Save Post');
?>