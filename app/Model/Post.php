<?php
class Post extends AppModel {
	public $validate = array(
		'title' => array(
            'rule' => 'notBlank'
        ),
        'post' => array(
            'rule' => 'notBlank'
        )
    );
}