<?php
class CommentsController extends AppController {
	public $helps = array('Html', 'Form');
	//public $components = array('Flash');
    public $uses = array('Post','comment');

    public function addcomment(){
        if ($this->request->is('post')) {
            $this->comment->create();
            $this->request->data['comment']['name'] = $this->Auth->user('username');
            $post_no = $this->request->data['comment']['posts_id'];
            if(!empty($this->request->data)){
                    //Check if image has been uploaded
                    if(!empty($this->data['comment']['upload']['name'])){
                        $file = $this->data['comment']['upload']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext)){
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['comment']['image'] = '/img/'.$file['name'];
                        }
                        //pr($this->request->data);
                        if ($this->comment->save($this->request->data)) {
                            $this->Flash->success(__('Your comment has been saved.'));
                            return $this->redirect(array('controller' => 'Posts','action' => 'view/'.$post_no));
                        }
                        $this->Flash->error(__('Unable to add your comment.'));
            
                    }
            }
        }
    }

    public function commentdelete($id,$p_no) {
        if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
        }

        if ($this->comment->delete($id)) {
            $this->Flash->success(
                __('The comment has been deleted.')
            );
        } else {
            $this->Flash->error(
                __('The comment with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('controller'=>'Posts','action' => 'view/'.$p_no));
    }

    public function editcomment($id = null) {
    	if (!$id) {
        throw new NotFoundException(__('Invalid comment'));
    	}

    	$post = $this->comment->findById($id);
    	if (!$post) {
        throw new NotFoundException(__('Invalid comment'));
    	}
    	if ($this->request->is(array('post', 'put'))) {
        	$this->comment->id = $id;
        	//pr ($this->comment->posts_id);
        	if ($this->comment->save($this->request->data)) {
            	$this->Flash->success(__('Your comment has been updated.'));
            	 return $this->redirect(array('controller' => 'Posts','action' => 'view/'.$post['comment']['posts_id']));
        	}
        	$this->Flash->error(__('Unable to update your commmet.'));
    	}

    	if (!$this->request->data) {
        	$this->request->data = $post;
    	}
	}
}