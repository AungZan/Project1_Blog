<?php
class PostsController extends AppController {
	public $helps = array('Html', 'Form',);
    public $helpers = array('Session');
	//public $components = array('Flash');
    public $uses = array('Post','comment');

	public function index() {
        $this->set('posts', $this->Post->find('all'));
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('posts', $post);
        $c=$this->comment->find('all', array('conditions' => array('posts_id' => $id)));
        $this->set('com', $c);
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            //pr($this->request->data);
            if(!empty($this->request->data))
                {
                    //Check if image has been uploaded
                    if(!empty($this->data['Post']['upload']['name']))
                    {
                        $file = $this->data['Post']['upload']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Post']['photo'] = '/img/'.$file['name'];
                        }
                        //pr($this->request->data);
                        if ($this->Post->save($this->request->data)) {
                        $this->Flash->success(__('Your post has been saved.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                    $this->Flash->error(__('Unable to add your post.'));
                    }

                    //now do the save
                    //$this->products->save($this->data) ;
                    
                }
            }
        }
    public function edit($id = null) {
    	if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    	}

    	$post = $this->Post->findById($id);
    	if (!$post) {
        throw new NotFoundException(__('Invalid post'));
    	}

    	if ($this->request->is(array('post', 'put'))) {
        	$this->Post->id = $id;
            if(!empty($this->data['Post']['upload']['name']))
                    {
                        $file = $this->data['Post']['upload']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Post']['photo'] = '/img/'.$file['name'];
                        }
                        else{
                            echo "Your picture extension is not allow.";
                        }
                    }
                    //pr($this->request->data);
        	if ($this->Post->save($this->request->data)) {
            	$this->Flash->success(__('Your post has been updated.'));
            	return $this->redirect(array('action' => 'index'));
        	}
        	$this->Flash->error(__('Unable to update your post.'));
    	}

    	if (!$this->request->data) {
        	$this->request->data = $post;
    	}
	}

	public function delete($id) {
        if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
                __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }

    /*public function addcomment(){
        if ($this->request->is('post')) {
            $this->comment->create();
            $this->request->data['comment']['name'] = $this->Auth->user('username');
            $post_no = $this->request->data['comment']['posts_id'];
            if ($this->comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been saved.'));
                return $this->redirect(array('action' => 'view/'.$post_no));
            }
            $this->Flash->error(__('Unable to add your comment.'));
            
        }
    }*/
}

